#! /usr/bin/env/python311

from vidstream import ScreenShareClient
import threading

if __name__ == '__main__':
    sender = ScreenShareClient('192.168.56.1', 9999)
    sender.start_stream()

    t = threading.Thread(target=sender.start_stream)
    t.start()

    while input("") != 'STOP':
        continue

    sender.stop_stream()
